SELECT COUNT(*) FROM Proteins;

SELECT * FROM Proteins LIMIT 10000;

SELECT * FROM Proteins WHERE NAME LIKE "%transcription factor%";

-- for blob and text which is too long we need to specify length
CREATE INDEX idx_name ON Proteins (name(15));

SELECT * FROM Proteins WHERE NAME LIKE "%transcription factor%";

ALTER TABLE Proteins drop index idx_name;